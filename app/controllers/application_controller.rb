class ApplicationController < ActionController::Base
  #This is to include SVG files
  # before_filter{ response.content_type = 'application/xhtml+xml' }
  
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
end
